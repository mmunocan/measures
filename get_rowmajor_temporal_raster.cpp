#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>

using namespace std;


void get_row_major_linearized_raster(const string &raster_filename, vector<int> &raster, size_t n_rows, size_t n_cols){
	ifstream raster_input_file(raster_filename, ios::binary);
	assert(raster_input_file.is_open() && raster_input_file.good());
	
	int value;
	for(size_t i = 0; i < n_rows * n_cols; i++){
		raster_input_file.read((char*)(& value), sizeof(int));
		raster[i] = value;
	}
	
	raster_input_file.close();
}

void read_rowmajor(const string &inputs_filename, const string &inputs_path, vector<int> &datasets){
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	string raster_input_filename;
	vector<int> raster(n_rows * n_cols);
	
	while(inputs_file >> raster_input_filename){
		raster_input_filename = inputs_path + "/" + raster_input_filename;
		get_row_major_linearized_raster(raster_input_filename, raster, n_rows, n_cols);
		for(size_t i = 0; i < n_rows * n_cols; i++) datasets.push_back(raster[i]);
	}
	
	inputs_file.close();
}

void write_rowmajor(const vector<int> &datasets, const string &output_filename){
	ofstream output_file(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	size_t n = datasets.size();
	
	int value;
	for(size_t i = 0; i < n; i++){
		value = datasets[i];
		output_file.write((char*)(& value), sizeof(int));
	}
	
	output_file.close();
}

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "usage: " << argv[0] << " <inputs_filename> <inputs_path> <output_filename>" << endl;
		return -1;
	}
	
	string inputs_filename = argv[1];
    string inputs_path = argv[2];
	string output_filename = argv[3];
	
	vector<int> datasets;
	read_rowmajor(inputs_filename, inputs_path, datasets);
	write_rowmajor(datasets, output_filename);	
	
	return 0;
}