CPP=g++

OBJECTS=
		
BINS=compute_runs_info get_zorder_temporal_raster get_rowmajor_temporal_raster continuous_alphabet compute_h0 compute_hk code_matrices
		
CPPFLAGS=-fopenmp -O9 -g3 -std=c++17 -march=native -mtune=native -mavx2 -funroll-loops -finline-functions -fomit-frame-pointer -flto -Wall -lm -DNDEBUG -I ~/include -L ~/lib 
CPPFLAGSB=-lsdsl -ldivsufsort -ldivsufsort64 
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

compute_runs_info:
	g++ $(CPPFLAGS) -o $(DEST)/compute_runs_info compute_runs_info.cpp $(OBJECTS) $(CPPFLAGSB) 
	
get_zorder_temporal_raster:
	g++ $(CPPFLAGS) -o $(DEST)/get_zorder_temporal_raster get_zorder_temporal_raster.cpp $(OBJECTS) $(CPPFLAGSB) 
	
get_rowmajor_temporal_raster:
	g++ $(CPPFLAGS) -o $(DEST)/get_rowmajor_temporal_raster get_rowmajor_temporal_raster.cpp $(OBJECTS) $(CPPFLAGSB) 
	
continuous_alphabet:
	g++ $(CPPFLAGS) -o $(DEST)/continuous_alphabet continuous_alphabet.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_h0:
	g++ $(CPPFLAGS) -o $(DEST)/compute_h0 compute_h0.cpp $(OBJECTS) $(CPPFLAGSB) 
	
compute_hk:
	g++ $(CPPFLAGS) -o $(DEST)/compute_hk compute_hk.cpp $(OBJECTS) $(CPPFLAGSB) 
	
code_matrices:
	g++ $(CPPFLAGS) -o $(DEST)/code_matrices code_matrices.cpp $(OBJECTS) $(CPPFLAGSB) 

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
