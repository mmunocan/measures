#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <climits>

using namespace std;

unsigned long interleave_uint32_with_zeros(unsigned int input)  {
    unsigned long word = input;
    word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
    word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
    word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
    word = (word ^ (word << 2 )) & 0x3333333333333333;
    word = (word ^ (word << 1 )) & 0x5555555555555555;
    return word;
}

unsigned long get_zorder(unsigned int n_rows, unsigned int n_cols){
	return interleave_uint32_with_zeros(n_cols) | (interleave_uint32_with_zeros(n_rows) << 1);
}

unsigned int get_k(unsigned int n_rows, unsigned int n_cols){
	if(n_rows >= n_cols){
		return  (1 << (32-__builtin_clz(n_rows-1)));
	}else{
		return  (1 << (32-__builtin_clz(n_cols-1)));
	}
}

void get_zorder_linearized_raster(const string &raster_filename, vector<int> &raster, size_t n_rows, size_t n_cols){
	ifstream raster_input_file(raster_filename, ios::binary);
	assert(raster_input_file.is_open() && raster_input_file.good());
	
	int value;
	size_t p;
	
	for(size_t i = 0; i < n_rows; i++){
		for(size_t j = 0; j < n_cols; j++){
			p = get_zorder(i,j);
			raster_input_file.read((char*)(& value), sizeof(int));
			raster[p] = value;
		}
	}
	
	raster_input_file.close();
	
}

void read_zorder(const string &inputs_filename, const string &inputs_path, vector<int> &datasets){
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	size_t k = get_k(n_rows, n_cols);
	
	string raster_input_filename;
	vector<int> raster(k*k, INT_MIN);
	
	while(inputs_file >> raster_input_filename){
		raster_input_filename = inputs_path + "/" + raster_input_filename;
		get_zorder_linearized_raster(raster_input_filename, raster, n_rows, n_cols);
		for(size_t i = 0; i < k*k; i++) datasets.push_back(raster[i]);
	}
	
	inputs_file.close();
}

void write_zorder(const vector<int> &datasets, const string &output_filename){
	ofstream output_file(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	size_t n = datasets.size();
	
	int value;
	for(size_t i = 0; i < n; i++){
		value = datasets[i];
		output_file.write((char*)(& value), sizeof(int));
	}
	
	output_file.close();
}

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "usage: " << argv[0] << " <inputs_filename> <inputs_path> <output_filename>" << endl;
		return -1;
	}
	
	string inputs_filename = argv[1];
    string inputs_path = argv[2];
	string output_filename = argv[3];
	
	vector<int> datasets;
	read_zorder(inputs_filename, inputs_path, datasets);
	write_zorder(datasets, output_filename);	
	
	return 0;
}