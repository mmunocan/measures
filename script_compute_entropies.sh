#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p $output_path
mkdir -p "$output_path/entropy_rowmajor"
mkdir -p "$output_path/entropy_zorder"

i=0
while IFS= read -r line
do
	set -- $line
		input_filename=$1
		input_path=$2
		raster_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./compute_h0 "$output_path/rowmajor/n_$raster_id.bin" > "$output_path/entropy_rowmajor/$output_result" &
		nohup ./compute_hk "$output_path/zorder/n_$raster_id.bin" > "$output_path/entropy_zorder/$output_result" &
		
	i=$(($i + 1))
done < $list_files