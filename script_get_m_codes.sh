#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p $output_path
mkdir -p "$output_path/m_codes"

i=0
while IFS= read -r line
do
	set -- $line
		input_filename=$1
		input_path=$2
		raster_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 0 "$output_path/m_codes/$raster_id-m0.bin" &
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 1 "$output_path/m_codes/$raster_id-m1.bin" &
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 2 "$output_path/m_codes/$raster_id-m2.bin" &
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 3 "$output_path/m_codes/$raster_id-m3.bin" &
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 4 "$output_path/m_codes/$raster_id-m4.bin" &
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 5 "$output_path/m_codes/$raster_id-m5.bin" &
		nohup ./code_matrices "$output_path/zorder/n_$raster_id.bin" 6 "$output_path/m_codes/$raster_id-m6.bin" &
		
	i=$(($i + 1))
done < $list_files