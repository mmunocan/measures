#include <iostream>
#include <fstream>
#include <cassert>
#include <map>
#include <cmath>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 4){
		cerr << "usage: " << argv[0] << " <input_filename> <m> <output_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	size_t m = atoi(argv[2]);
    string output_filename = argv[3];
	
	ifstream input_file(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * datas = new int[n];
	
	input_file.read((char*)datas, n * sizeof(int));
	input_file.close();
	
	size_t k = pow(pow(2, m), 2);
	size_t t = n / k;
	
	int * output_datas = new int[t];
	
	map<string, int> submatrices;
	string submatrix;
	int index = 1;
	for(size_t i = 0; i < t; i++){
		submatrix.assign(((char*)datas) + (i*k*sizeof(int)), (k*sizeof(int)));
		if(submatrices.count(submatrix) == 0){
			submatrices[submatrix] = index;
			output_datas[i] = index;
			index++;
		}else{
			output_datas[i] = submatrices[submatrix];
		}
	}
	
	ofstream output_file(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)output_datas, t * sizeof(int));
	output_file.close();
	
	delete [] datas;
	delete [] output_datas;
	return 0;
}