#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <iomanip>

#include <sdsl/vectors.hpp>
#include <sdsl/bit_vectors.hpp>

using namespace std;
using namespace sdsl;

int main(int argc, char ** argv){
	if(argc != 2){
		cerr << "usage: " << argv[0] << " <input_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
    
	ifstream input_file;
	input_file.open(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * datasets = new int[n];
	
	input_file.read((char*)datasets, n * sizeof(int));
	input_file.close();
	
	vector<int> runs_vals;
	vector<bool> runs_bits;
	runs_vals.push_back(datasets[0]);
	runs_bits.push_back(true);
	
	for(size_t i = 1; i < n; i++){
		if(datasets[i] != datasets[i-1]){
			runs_vals.push_back(datasets[i]);
			runs_bits.push_back(true);
		}else{
			runs_bits.push_back(false);
		}
	}
	
	size_t r = runs_vals.size();
	int_vector<32> runs_vals_c(r);
	for(size_t i = 0; i < r; i++) runs_vals_c[i] = runs_vals[i];
	bit_vector runs_bits_c(n, 0);
	for(size_t i = 0; i < n; i++) runs_bits_c[i] = runs_bits[i];
	
	rank_support_v<1> rank_c(&runs_bits_c);
	
	cout << r << ";" << size_in_mega_bytes(runs_vals_c) << ";" << size_in_mega_bytes(runs_bits_c) << ";" << size_in_mega_bytes(rank_c) << endl;
	
	delete [] datasets;
	
	return 0;
}