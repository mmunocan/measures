#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p $output_path
mkdir -p "$output_path/zorder"
mkdir -p "$output_path/rowmajor"

i=0
while IFS= read -r line
do
	set -- $line
		input_filename=$1
		input_path=$2
		raster_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./get_zorder_temporal_raster $input_filename $input_path "$output_path/zorder/$raster_id.bin" &
		nohup ./get_rowmajor_temporal_raster $input_filename $input_path "$output_path/rowmajor/$raster_id.bin" &
		
	i=$(($i + 1))
done < $list_files