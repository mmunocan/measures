#include <iostream>
#include <iomanip>

#include <sdsl/suffix_trees.hpp>
#include <sdsl/wt_int.hpp>

using namespace std;
using namespace sdsl;

int main(int argc, char ** argv){
	if(argc != 2){
		cerr << "usage: " << argv[0] << " <input_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
	
	cst_sct3<csa_wt<wt_int<>>> cst;
	construct(cst, input_filename, sizeof(int));
	
	cout << input_filename << ";";
	cout << get<0>(Hk(cst, 0)) << endl;
	
	return 0;
}