#include <iostream>
#include <fstream>
#include <cassert>
#include <climits>
#include <map>
#include <algorithm>
#include <vector>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 3){
		cerr << "usage: " << argv[0] << " <input_filename> <output_filename> " << endl;
		return -1;
	}
	
	string input_filename = argv[1];
    string output_filename = argv[2];
	
	ifstream input_file(input_filename, ios::binary);
	assert(input_file.is_open() && input_file.good());
	
	input_file.seekg(0, ios::end);
	size_t n = input_file.tellg() / sizeof(int);
	input_file.seekg(0, ios::beg);
	
	int * datas = new int[n];
	
	input_file.read((char*)datas, n * sizeof(int));
	input_file.close();
	
	// Get all symbols
	map<unsigned int, unsigned int> symbols;
	for(size_t i = 0; i < n; i++){
		if(symbols.count(datas[i]) == 0){
			symbols[datas[i]] = 1;
		}
	}
	
	// sort symbols
	vector<unsigned int> sorted_symbols;
	for(auto it : symbols){
		sorted_symbols.push_back(it.first);
	}
	sort(sorted_symbols.begin(), sorted_symbols.end());
	
	// assign new symbols
	map<unsigned int, unsigned int> dictionary;
	unsigned int s = 1;
	for(unsigned int c : sorted_symbols){
		dictionary[c] = s;
		s++;
	}
	
	// generate new text
	unsigned int * output = new unsigned int[n];
	for(size_t i = 0; i < n; i++){
		output[i] = dictionary[datas[i]];
	}
	
	ofstream output_file(output_filename, ios::binary);
	assert(output_file.is_open() && output_file.good());
	output_file.write((char*)output, n * sizeof(unsigned int));
	output_file.close();
	
	delete [] datas;
	delete [] output;
	return 0;
}